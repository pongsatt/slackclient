A very simple slack chat api call via docker image.

## Usage
```sh
docker run --rm -e SLACK_TOKEN=<token> gitlab.com/pongsatt/slackclient 'mychannel' 'Hello world!'
```

## Release
```sh
git tag -a vx.y -m "<desc>"
git push --tags
```