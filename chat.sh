#!/bin/sh

CH=$1
TEXT=$2

curl -s -X POST https://slack.com/api/chat.postMessage \
      --data-urlencode "channel=#$CH" \
      --data-urlencode "text=$TEXT" \
      --data-urlencode "token=$SLACK_TOKEN"