FROM alpine

WORKDIR /usr/bin
COPY chat.sh chat
RUN \
    apk add --no-cache curl && \
    chmod +x chat

ENTRYPOINT ["chat"]